OVPN_DATA="ovpn-data"

start-ntop:
		docker run --net=host --name ntop -d -it lucaderi/ntopng-docker ntopng -v

start-rtorrent:
		docker run -it --rm -p 8089:80 -p 45566:45566 -p 9527:9527/udp --dns 8.8.8.8 -v /var/www/tm/rudown/:/rtorrent kfei/docktorrent
start-rtorrent-always:
		docker run -it --restart always -p 8089:80 -p 45566:45566 -p 9527:9527/udp --dns 8.8.8.8  -v /var/www/tm/rudown/:/rtorrent kfei/docktorrent

create-portainer:
		docker run -d -p 9000:9000 -v "/var/run/docker.sock:/var/run/docker.sock" portainer/portainer

start-portainer:
		docker run portainer

ovpn-init:
		docker volume create --name $OVPN_DATA
		docker run -v $OVPN_DATA:/etc/openvpn --rm kylemanna/openvpn ovpn_genconfig -u udp://tcial.org
		docker run -v $OVPN_DATA:/etc/openvpn --rm -it kylemanna/openvpn ovpn_initpki

ovpn-start:
		docker run --restart always -v $OVPN_DATA:/etc/openvpn -d -p 1194:1194/udp -p 1194:1194/tcp --cap-add=NET_ADMIN kylemanna/openvpn

ovpn-get:
#ifndef NAME
#  $(error NAME is undefined)
#endif
		docker run -v $OVPN_DATA:/etc/openvpn --rm -it kylemanna/openvpn easyrsa build-client-full $(NAME) nopass
		docker run -v $OVPN_DATA:/etc/openvpn --rm kylemanna/openvpn ovpn_getclient $(NAME) > $(NAME).ovpn

start-stackedit:
		echo "see dev/stackedit/Makefile"

start-etherpad-always:
		docker run --name etherpad --restart unless-stopped -d -p 9001:9001 etherpad/etherpad:1.8.0

start-couchbase:
		docker run -d --restart always --name couchdb -e COUCHDB_USER=admin -e COUCHDB_PASSWORD=arstoeansrt1232taarst -p 16984:6984 -v /etc/letsencrypt/live/tcial.org/cert.pem:/etc/couchdb/cert/couchdb.pem -v /etc/letsencrypt/live/tcial.org/privkey.pem:/etc/couchdb/cert/privkey.pem -v /etc/letsencrypt/live/tcial.org/chain.pem:/etc/ssl/certs/ca-certificates.crt -v $PWD/etc:/opt/couchdb/etc/local.d -v $PWD/data:/opt/couchdb/data couchdb:2
