See makefile for build and run.

Confluent version with schema registry.

Trick to get kafka and working with external access without hostname passing is:
set up two listeners: one for internal-internal communication, and a second one for internal-external communication
see server.properties for the relevant entries.
Also has to be run with the hostname specified and a /etc/hosts redirect to localhost
