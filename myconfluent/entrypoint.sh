#!/usr/bin/env bash
TIMEOUT=${TIMEOUT:-5}
kafka/bin/zookeeper-server-start /kafka/etc/kafka/zookeeper.properties > /kafka/logs/zookeeper.log &
kafka/bin/kafka-server-start /kafka/etc/kafka/server.properties &
sleep $TIMEOUT
kafka/bin/schema-registry-start /kafka/etc/schema-registry/schema-registry.properties
