Docker registry

Setup using proxy and reverse proxy in apache2:

```
ProxyPass /v2 http://0.0.0.0:5000/v2
ProxyPassReverse /v2 http://0.0.0.0:5000/v2

# doesn't work without those:
Header add X-Forwarded-Proto "https"
RequestHeader add X-Forwarded-Proto "https"
```

Can't map the docker registry to any other path but root. Or have to use a subdomain.
Can change the port though.


Usage:

```
# login
docker login https://tcial.org

# tag the image
docker tag myshark:latest tcial.org/mybuntu:20

# push the image
docker push tcial.org/mybuntu:20

# pull and use the image
docker pull tcial.org/mybuntu:20
```
