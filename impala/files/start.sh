#!/bin/bash
/start-hdfs.sh
/start-impala.sh
echo "Impala is started"
until impala-shell -q "select 1"
do
    echo "not ready"
    sleep 1
done
echo "Impala is ready"
