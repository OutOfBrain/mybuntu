
Ubuntu based mynord which contains s6 service.
Self-made rebuild of `mynord` based on ubuntu to be able to use `nordvpn` command directly.


Sample call to download a random video from youtube:

    docker run --rm -it -v $PWD:/w --privileged -e COUNTRY=de -e CMD="youtube-dl 'https://www.youtube.com/watch?v=8xQakng5po0'" mynord2:latest

more complicated one:

    docker run --rm -it -v $PWD:/w --privileged -e COUNTRY=de -e CMD="youtube-dl --cache-dir /tmp/.ytdlcache --extract-audio -o 'files/$sessionId-%(title)s-%(id)s.mp3' --audio-format mp3 'https://www.youtube.com/watch?v=8xQakng5po0' 2>&1" mynord2:latest

