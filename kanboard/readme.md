Kanboard

To build - use oficially image or just build from latest github and build docker image.

Installed on apache via reverse proxy:

        ProxyPass /kb http://0.0.0.0:9998
        ProxyPassReverse /kb http://0.0.0.0:9998

To create a backup and keep it to 5 versions:

    cp db.sqlite "db_backup_$(date +%Y-%m-%d_%H:%M:%S).sqlite" ; (ls -t db_backup_* | tail -n +6 | xargs rm -f)
