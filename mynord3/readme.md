Barebones mynord based on mybuntu:20

This is probably the best version currently as it directly uses the `nordvpn` command and the nordlynx protocoll.
The downside is the slow login process which would be faster in the original mynord.

Error message

> Whoops! /run/nordvpn/nordvpnd.sock not found


Is caused by nordvpn service not being started yet. The service initializes that socket.
Probably didn't start up properly or needs some additional wait time.


Error message

> connection error: desc = "transport: Error while dialing dial unix /run/nordvpn/nordvpnd.sock: connect: no such file or directory"

Is caused by too quick startup. Probably needs additional wait time.


To actually provide https wrap an nginx on the outside with proxypass.
Have not found a relyable way to get access to the inner container otherwise and this is way easier.

```
location /docktorrent2/ {
        proxy_pass http://localhost:8088/;
}
```
