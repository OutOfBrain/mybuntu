#!/usr/bin/env bash

set -xe
mkdir -p /run/nordvpn/

/etc/init.d/nordvpn start
echo "nordvpn daemon started"

if [ -z "$SLEEP" ];
  then SLEEP=0.8
fi

sleep $SLEEP

if [ -z "$COUNTRY" ];
  then COUNTRY=de
fi

if [ -z "$TECHNOLOGY" ];
  # or OpenVPN
  then TECHNOLOGY=NordLynx
fi
echo "parameters COUNTRY: " $COUNTRY " TECHNOLOGY: " $TECHNOLOGY
#nordvpn set killswitch 1
nordvpn set technology $TECHNOLOGY

trap "echo exit-logout; nordvpn logout" EXIT
set +xe
nordvpn login --username outofbrain@gmail.com --password verysecureconnection1
set -xe
nordvpn connect $COUNTRY

#if [ ! -z "$FIXKILLSWITCH" ];
#then
#  # fix killswitch for builtin server / outside server communication
#  iptables -I INPUT 1 -p tcp -m tcp --dport 80 -j ACCEPT
#  iptables -I OUTPUT 1 -p tcp --sport 80 -j ACCEPT
#fi

echo "startup done"
bash
