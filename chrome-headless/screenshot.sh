#!/usr/bin/env bash
export

chromium-browser --headless --disable-gpu --screenshot --window-size=1280,1696 --no-sandbox "$WEBSITE"
mv screenshot.png /output/$OUTPUT
