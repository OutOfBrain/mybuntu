#!/usr/bin/env bash

kafka/bin/zookeeper-server-start.sh /kafka/config/zookeeper.properties > /kafka/logs/zookeeper.log &
kafka/bin/kafka-server-start.sh /kafka/config/server.properties
