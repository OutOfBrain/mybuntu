#!/usr/bin/env bash

set -xe

# copy config over as in case of volume mount
rm -f /config/rtorrent/rtorrent_sess/*.lock

ln -s /usr/bin/php /usr/bin/php7

# setup directories, permissions and start rtorrent in background
/usr/bin/rtorrent.sh

## generate self signed keys
#SUBJECT="/C=DE/ST=NI/L=B/O=Tcial.org/OU=ru Server/CN=*"
#if [[ -f /config/keys/cert.key && -f /config/keys/cert.crt ]]; then
#    echo "using keys found in /config/keys"
#else
#    echo "generating self-signed keys in /config/keys, you can replace these with your own keys if required"
#    openssl req -new -x509 -days 3650 -nodes -out /config/keys/cert.crt -keyout /config/keys/cert.key -subj "$SUBJECT"
#fi

service nginx start
service php7.4-fpm start

bash /usr/bin/startup-nord.sh
# FIXKILLSWITCH=1 bash /usr/bin/startup-nord.sh
