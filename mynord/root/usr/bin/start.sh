if [ -n "$SERVER" ];
  # check if a sever is set
  then RANDOM_SERVER="/etc/ovpn_tcp/${SERVER}.nordvpn.com.tcp.ovpn" ;
elif [ -n "$COUNTRY" ];
then
  # if not check if a country is set
  RANDOM_SERVER=`ls /etc/ovpn_tcp/$COUNTRY* | shuf -n 1`
else
  # otherwise select random server if one was not passed to script via $SERVER
  RANDOM_SERVER=`ls /etc/ovpn_tcp/* | shuf -n 1`
fi

if [ ! -f "$RANDOM_SERVER" ];
  # check if config actually exists
  then echo "$RANDOM_SERVER does not exist" ;
  exit ;
fi;

echo connecting to $RANDOM_SERVER
openvpn --config $RANDOM_SERVER --auth-user-pass /etc/ovpn_auth > /var/log/ovpn.log &
(sleep 3 && echo "nameserver 8.8.8.8" > /etc/resolve.conf ) &
#bash -c "echo nameserver 8.8.8.8 > /etc/resolv.conf"
#echo hello world
#echo nameserver 8.8.8.8
echo nameserver 8.8.8.8 > /etc/resolv.conf
cat /etc/resolv.conf

(
sleep 5 ; curl https://ifconfig.me/
# check for errors during connection
if cat /var/log/ovpn.log | grep error || cat /var/log/ovpn.log | grep SIGTERM;
then
  # there was a match containing "error"
  echo "CONNECTION FAILED!"
  exit # can't actually exit the parent shell it seems - so just a warning
fi
if [ -n "$CMD" ];
then
  bash -c "$CMD"
  echo "done with command execution - exiting"
  kill -9 0
fi
) &
bash
